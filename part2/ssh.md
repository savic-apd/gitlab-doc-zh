完成账户注册即拥有使用账户和密码通过 *https://* 协议访问GitLab中Git仓库的权限。可以通过配置SSH，使我们在 *clone* 仓库时不用验证账户和密码。

下文提到的终端，在Windows中可以在右键菜单点击Git Bash Here打开。需要[安装Git](./install-git.md)。也可以通过[Git工具](../part3/)进行SSH key的生成。

## GitLab支持的SSH key类型

| 算法                | 公钥(Public key)  | 私匙(Private key) |
| :------------------ | :---------------- | :---------------- |
| ED25519 (preferred) | id_ed25519.pub    | id_ed25519        |
| ED25519_SK          | id_ed25519_sk.pub | id_ed25519_sk     |
| ECDSA_SK            | id_ecdsa_sk.pub   | id_ecdsa_sk       |
| RSA (2048-bit size) | id_rsa.pub        | id_rsa            |
| DSA (deprecated)    | id_dsa.pub        | id_dsa            |
| ECDSA               | id_ecdsa.pub      | id_ecdsa          |

## 检查是否存在SSH keys

1. 在终端输入 `ls -al ~/.ssh` 查看是否存在SSH keys
   - 如果存在，将看到SSH key pair，例如 *id_rsa.pub* 和 *id_rsa*
   - 如果发生错误，则说明在SSH默认的目录下不存在SSH key pair。
2. 生成一个新的SSH key，或者将已经存在的SSH key添加到账户中

## 生成新的SSH key并添加进ssh-agent

1. 在终端输入 `ssh-keygen -t ed25519 -C "your_email@example.com"` ，请将邮箱地址替换为GitLab账户邮箱。
   - 在会话中选择文件目录，默认为 *~/.ssh/algorithm*
        ```
        > Enter file in which to save the key (/home/mengze/.ssh/id_ed25519):
        ```
   - 在会话中设置密码，密码可以为空。可以一直按回车键来完成SSH key的生成。一直按回车键SSH key将安装在默认位置，密码为空。
        ```
        > Enter passphrase (empty for no passphrase): [Type a passphrase]
        > Enter same passphrase again: [Type passphrase again]
        ```
3. 在终端中输入 `eval "$(ssh-agent -s)"`，开启ssh-agent。如果返回以下信息则说明开启成功
    ```
    > Agent pid 59566
    ```
3. 在终端中输入`$ ssh-add ~/.ssh/id_ed25519`，将刚刚生成的SSH key添加到ssh-agent中

## 将SSH key添加进账户
1. 在终端中输入 `cat ~/.ssh/id_ed25519.pub`，查看刚刚生成的公钥。复制命令的输出信息
2. 登录GitLab，点击右上角的头像，选择 **Preferences**。在左侧边栏中选择 **SSH Keys**
3. 在 **Key** 文本框中粘贴复制的公钥信息。请确保输出了完整公钥。
4. 在 **Title** 文本框中输入设备标识
5. **Expires ad** 文本框中可以为空
6. 点击 **Add key** 完成公钥添加

## 测试SSH链接
验证公钥是否添加成功。

1. 在终端中输入 `ssh -T git@gitlab.example.com`，替换`gitlab.example.com`为我们的GitLab地址。如果第一次连接，将提示验证GitLab地址。在下边会话中输入yes，或按回车键
    ```
    The authenticity of host 'gitlab.example.com (ip address)' can't be established.
    ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added 'gitlab.example.com' (ECDSA) to the list of known hosts.
    ```
2. 再次在终端中输入 `ssh -T git@gitlab.example.com`，将会收到一个Welcome消息。