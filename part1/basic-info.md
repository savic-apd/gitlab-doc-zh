# GitLab基本管理要素
### 概述
GitLab（15.0）是一个ALL-in-ONE的平台，集成了版本控制、文件存储、权限管控、评审和审批、团队协作、项目进度管理、自动化集成和测试、软件发布等功能。相当于轻量化缩水版的C919 JIRA+SVN+Bamboo+Windchill体系，但更具灵活性，适合于工具开发。如果想用于重量级的项目开发，则需要在此基础上做定制，以匹配各严格的体系要求。

GitLab集成了很多功能，大部分都是提供了基本功能。用户可以根据自己的情况定制一些模版或者流程。这就需要工具团队定义好工具的开发流程和各类模版，再自己配置GitLab。

GitLab背后的设计和管理逻辑还是软件工程，这对于我们做过大型项目的工程师来说比较容易理解。为了保证使用的灵活性，其管理要素和功能元素都是比较基本的，然后通过复杂的、用户定制的关联设计来达成目标。这就要求平台管理者深入理解其设计理念，以匹配其实际研发应用场景和要求。所以，GitLab的前期学习成本和建设成本比较高，需要团队做好规划，编制易懂的使用手册。

### 基本概念
|概念|含义与用途|
|---|---|
|[Group](#1.Group)|用于管理人员、项目组织|
|[Project](#2.Project)|用于管理研发项目，包括仓库管理、项目管理、CI/CD等方面的工具|
|[Issue](#3.Issue)|议题，用于管理跟踪一个功能特性的开发、新想法的实现或一个问题的修复，超出字面意义上的“问题”。|
|[Label](#4.Label)|标记，可以给Issue、Project或MR等打上标记，便于搜索过滤管理。其更强大的功能是用于Issue面板，定制各类看板。|
|[Epic](#5.Epic)|用于夸项目管理，收费功能。|
|[Merge Request](#6.MR)|用于合并分支，集成审批、评审、CICD等功能。|
|[Release](#7.Release)|用于创建发布镜像或软件包。|
|[CI/CD](#8.CI/CD)|持续集成、持续发布和持续部署。持续部署是与云平台的衔接的。|

### 基本工作流程
由于Git的管理特性和SVN有较大的差异，Git拥有更强大的分支设计，所以推荐开发人员多创建和使用分支。
GitLab也在管理仓库和开发流程时也继承了这一特性。所有的变更要先在分支中完成，再通过一系列的检查、评审和测试后合并到目标分支中去。在这个过程中，Git分支和GitLab的MR是绑定一起走流程的，详细可见[6. Merge Request](#6.MR)和[9. GitLab工作流](#9-GitLab-Git)。
其基本流程可参考下图。

![GitLab workflow](./pic/gitlab_workflow_example_11_9.png)

### 中英文对照表和缩略语表
- 中英文对照表

|英文|中文|
|---|---|
|Project|项目|
|Issue|议题|
|Label|标签|
|Epic|史诗|
|Repositories|仓库|
|Branch|分支|

- 缩略语对照表

|缩略语|全称|
|---|---|
|MR|Merge Request|
|CI|Continuous Integration|
|CD|Continuous Delivery|
|CVE|Common Vulnerabilities & Exposures|

<a name="1.Group"></a>
## 1. Group
Gitlab使用Group同时管理一个或多个相关项目：
- Group内项目的权限管理。如果某人有权访问该Group，则他们可以访问该Group中的所有项目。
- GitLab支持查看Group中项目的所有Issue和合并请求，并查看显示Group活动的分析。
- 使用Group同时与Group的所有成员进行通信。
- 支持Group-subGroup层次关系。

<a name="2.Project"></a>
## 2. Project
GitLab使用项目来托管代码库，用项目这个管理容器来跟踪Issue、规划工作、协作处理代码，以及持续构建、测试和使用内置 CI/CD 来部署应用程序。
项目有以下特性：
### （1）仓库
<a name="Issue跟踪"></a>
- Issue跟踪：
  - Issue面板：组织和优化工作流，用于策划、组织功能特性研发或产品发布任务，同时支持可视化工作流。和敏捷的看板、Scrum类似。
  - 多Issue面板：一个项目中可以创建多个Issue面板，支持不同团队不同视角的Issue管理。
- 仓库：使用Git管理项目代码和文档数据
  - 分支：使用Git的分支策略来管理代码。
  - 分支保护：可配置策略防止开发者在没有同行评审情况下更改历史记录或上传（push）代码。
  - Tag保护：使用用户权限策略管控Tag，防止未授权的用户更改或删除Tag。
  - 仓库镜像映射：Git拉取（pull）外部仓库。
  - **上传代码保护：通过GPG加密策略识别用户上传。**
  - 访问令牌(Deploy Token)：控制用户访问仓库的权限、时间等。
- Web IDE：支持在线编辑文件。
- CVE ID申请：支持在披露漏洞平台申请CVE ID。你的用户可以通过在平台上检索，查到使用产品的漏洞和缺陷，并实施相应的保护策略。此功能只适用于GitLab的公开项目。
### （2）Issue和MR
- [Issue跟踪](#Issue跟踪)
- [Merge Request](#6.MR)：
  - 审批：通过审批后再创建分支、实施更改。
  - GitLab UI支持线上处理合并冲突。
  - 更改评审：合并到主分支前，须通过评审。
- Label：通过标签管理多个Issue和MR。
- 时间跟踪管理：跟踪记录Issue和MR的预计时间和实际花费时间。
- Milestones管理
- 信息模版：定制Issue和MR的信息描述模版。
- 快速识别命令：GitLab可以识别文本中所描述的命令，例如/close、/attention@user1等。
- 自动填词补充
- Web IDE
### （3）GitLab CI/CD
- Gitlab CI/CD：GitLab自建持续集成、交付和部署工具。
  - 容器注册：创建并推送Docker镜像
  - 自动部署
  - 启用/关闭GitLab CI/CD
  - 流水线（Pipelines）：支持在UI中配置和可视化CI/CD流水线。
    - 规划流水线启动时间
    - 可视化流水线图
    - 任务（Job Artifacts）
    - 流水线配置
  - K8s云集成
  - 特性标记（Feature Flags）：支持小批量定向发布新特性，例如控制向特定用户子集发布新功能。控制新特性的测试、使用风险。GitLab使用Unleash管理特性标记。
- GitLab页面：支持创建、测试并部署静态页面。
### （4）其他特性
- Wiki：在GitLab中维护项目Wiki页面。
- Snippets：GitLab使用Snippets存储、共享代码片段。
- 价值流分析（Value streadm analytics）：评审分析软件开发阶段的时间、成本和价值。付费功能。
- 项目数据统计（Insights）：收集、统计项目各个维度的信息，例如Issue数量、MR关闭周期等等。
- 安全控制面板：配置GitLab的自动扫描功能，检查项目各类缺陷。
- 语法高亮
- **徽章**
- 发布（Releases）
- **包注册管理**
- Code Owners
- License管理
- 依赖清单管理
- **需求管理**：可以理解为产品行为规范或用户需要管理。
- 智能代码：类似IDE代码类型标签、导航功能。

<a name="3.Issue"></a>
## 3. Issue
GitLab使用Issue来协作提出想法、解决问题和计划工作。与团队和外部合作者分享和讨论提案。Issue始终与特定项目相关联。
可以将Issue用于多种用途，根据需求和工作流程进行定制。
- 讨论一个想法的实施。
- 跟踪任务和工作状态。
- 接受功能建议、问题、支持请求或错误报告。
- 详细说明代码实现。

Issue重点在于具体实施前在公共平台分享、讨论并明确想法与思路，这也是开源项目开发的特性。
Issue与Label、Epic之间的关系好理解，在[6. Merge Request](#MR)中会重点介绍下Issue和MR之间的关系。

<a name="4.Label"></a>
## 4. Label
随着IssueIssue、MR合并请求和Epic史诗的数量不断增加，跟踪这些项目变得越来越具有挑战性。它们可以帮助组织和标记的工作，以便可以跟踪和找到感兴趣的工作项目。

标记是Issue看板的关键部分。使用标记，可以：
- 使用颜色和描述性标题（如 bug、feature request 或 docs）对Epic史诗、IssueIssue和MR合并请求进行分类。
- 动态过滤和管理Epic史诗、IssueIssue和MR合并请求。

标准的看板可以通过“to-do”、“processing”和“closed”三个label定义三个list（泳道），每个贴上label的Issue按序分列在各自的list（泳道）中。
GitLab也可以通过label定制工作流程的不同阶段，例如“reported”、“analyzed”、“assigned”、“processing”和“closed”。然后创建新的Issue面板，通过label筛选定义这5个不同的list（泳道）。

<a name="5.Epic"></a>
## 5. Epic
此功能收费，暂不适用。
当IssueIssue跨项目和里程碑共享一个主题时，可以用Epic史诗来管理它们。可以为Epic史诗指定开始和结束日期，GitLab将为创建一个可视化的路线图（RoadMap）来查看进度。
以下是Epic的适用场景：
- 当团队在做一个大的功能时，涉及到在一个群组中的不同项目中的不同问题的多个讨论。
- 跟踪该组问题的工作何时开始和结束。
- 要在高层次上讨论并协作相关功能的想法和范围。


<a name="6.MR"></a>
## 6. Merge Request
合并请求 (MR) 是源代码更改进入到分支中的方式。 当您打开合并请求时，您可以在合并之前对代码更改进行可视化和协作。 合并请求包括：
- 合并请求的描述。
- 代码更改，支持内联代码审查。
- 有关 CI/CD 流水线的信息。
- 评论列表。
- 历次提交列表。

合并请求 (MR) 是 GitLab 作为代码协作和版本控制的基础。MR是基于 Git 的分支策略来协作处理代码，既可以理解MR==Git分支，但Git分支！=MR。因为Git分支同时可以用作其他用途，例如创建tag发布等等。
仓库由其默认分支（Master）组成，其中包含代码库的主要版本，可以从中创建次要分支，也称为功能分支，以提出对代码库的更改，而不会将它们直接引入代码库的主要版本。在与他人协作时，分支尤其重要，避免在没有事先审核、测试和批准的情况下将更改直接推送到默认分支。当创建新的功能分支、更改文件并将其推送到GitLab时，可以选择创建合并请求，这本质上是将一个分支合并到另一个分支的请求。
将更改添加到的分支称为源分支，而您请求将更改合并到的分支称为目标分支。目标分支可以是默认分支或任何其他分支，取决于具体的分支策略。
在合并请求中，除了可视化原始内容和提议的更改之间的差异之外，还可以在结束工作和合并合并请求之前执行大量任务。

从Issue中可以轻松关联MR，但从已经创建的MR中关联Issue的动作是隐性的，没有专门的按钮操作或提示。可以通过在MR的描述中提到Issue的id “#id“的方式关联上Issue，这样可以在Issue的页面中看到关联的MR。
Gitlab也提供了特殊的智能化活动识别，如果在MR描述中包含“close #id”、“fix #id”、“resolve #id”或“implement #id”等字眼，在合并MR时，GitLab会自动关闭提到的Issue。即使当前的Issue包含多个MR，它也会被关闭。当然可以再reopen误关闭的Issue，但这样会提高操作成本和出错的概率，所以这几个字眼要慎用。

MR的工作流程：
1. 创建仓库（Git）分支 &&（1.2）创建MR。任务并行，GitLab webIDE中创建分支时，即可创建对应的MR。创建MR时，即要求有新的Git分支。
2. MR中进行review、approve等操作。
3. 确认更改完成后，合并（Merge）掉此MR。
4. 如果想暂停更改，不要删除此MR，可以先关闭（Close）MR。随后在任何时间点都可以再次激活（Open）MR，继续更改活动。关闭MR操作比删除MR更灵活。

<a name="7.Release"></a>
## 7. Release
GitLab使用Git tag功能创建项目的快照，即发布，发布中可以包含安装包和发行说明。GitLab支持在任何分支上发布软件。发布还会创建一个Git tag来标记源代码中的发布点。
请注意虽然git支持给任何的commit创建tag，但GitLab推荐在分支上创建tag。通常可以通过GitLab的tag页面、release页面或者CI/CD页面创建发布。

一个发布可以包括：
- 仓库源代码的快照
- ***通用包***
- 与已发布代码相关联的其他数据
- 发行说明
  
创建发布时，GitLab会自动创建以下内容：
- 源代码以及相关数据的压缩包
- Json格式的发布文件清单

同时，用户也可以自行补充信息：
- 发布说明
- 添加Git tag说明信息
- 关联Milestone
- 添加其他源码或者文件附件以及链接

**重点在于如何通过CI/CD创建发布，将开发、集成、测试和发布衔接在一起。**

<a name="8.CI/CD"></a>
## 8. CI/CD
TBD

<a name="9-GitLab-Git"></a>

## 9. GitLab工作流
GitLab服务器内置了Git服务器，用户可以直接从服务器上clone对应的项目。

### （1） 准备环境
在开始之前先确定满足一下条件：
- 本地有Git客户端
- 确保有访问GitLab项目的权限
- 有SSH密钥，并已经添加到GitLab账户中

### （2）clone项目
通过一下页面找到对应项目的URL。
![GitLab中Project的repo](./pic/Project-Git-repo.jpeg)
在通过git命令行clone对应的项目。
```shell
git clone git@gitlab.com:savic-apd/gitlab-doc-zh.git
```

### （3）本地编辑更新
建议通过git创建自己的本地分支，并在分支中进行对应的变更。不建议直接在主分支中进行变变更。
```shell
git switch Add-GitLab-intro-document
#  Add, modify or delete files.
git add .
git commit -m "add introduction"
```

### （4）提交本地分支至GitLab
通过如下命令提交本地分支，通过命令行显性表示本地分支和服务器目标分支。如果服务器没有Add-introduction这个分支，GitLab也会新建此分支。
```shell
git push origin Add-GitLab-intro-document:Add-introduction
```
由于GitLab将Git分支和MR关联的很紧，分支合并到主分支中必须通过MR流程，所以可以从本地的终端日志中看到GitLab的提醒信息。
```shell
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 2 threads
Compressing objects: 100% (8/8), done.
Writing objects: 100% (8/8), 562.16 KiB | 8.39 MiB/s, done.
Total 8 (delta 0), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for Add-introduction, visit:
remote:   https://gitlab.com/savic-apd/gitlab-doc-zh/-/merge_requests/new?merge_request%5Bsource_branch%5D=Add-introduction
remote: 
To gitlab.com:savic-apd/gitlab-doc-zh.git
 * [new branch]      Add-GitLab-intro-document -> Add-introduction
```
此时可以在GitLab项目页面中看到分支信息，也会看到GitLab给出很多提示，建议创建对应的MR。
![New Branch and MR Notification](./pic/new-branch-and-see-MR-notification.jpeg)

<a name="10-SSH"></a>

## 10. SSH
GitLab使用SSH安全协议来保护对Git服务端的访问。当在GitLab中配置好用户的SSH私钥后，后续用户不用每次访问Git服务器时都输入用户名和密码。
### （1） 准备环境
- openSSH客户端
- GNU/Linux，macOS或Windows 10的操作系统
- SSH 6.5或更高版本，通过ssh -V命令查询
### （2） 创建SSH密钥
输入以下命令创建密钥：
```shell
ssh-keygen -t rsa -b 2048 -C "comments"
```
按提示顺序指定密钥存放路径和密码。
### （3）在GitLab中登记密钥
将生成的*.pub文件中的内容拷贝至GitLab个人账户配置的SSH key页面中。
![User-ssh](./pic/User-SSH.jpeg)
其中，**Key**中填写的内容格式如下：
```
ssh-rsa * comments
```
